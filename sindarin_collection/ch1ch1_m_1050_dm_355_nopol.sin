############################################################
# chargino pairs for stub tracks at 3 TeV CLIC
# ulrike.schnoor@cern.ch
############################################################
model = MSSM

# chargino
mch1   = 1050         # chargino1 mass (signed)


# neutralino
mneu1 = 1049.84

#other SUSY particles
mch2   = 10000	      # chargino2 mass (signed)
mneu2  = 10000	      # neutralino2 mass (signed)
mneu3  = 10000	      # neutralino3 mass (signed)
mneu4  = 10000	      # neutralino4 mass (signed)
mHH    = 10000
mHA    = 10000
mHpm   = 10000
msd1   = 10000	      # d-squark mass
msc1   = 10000	      # c-squark mass
mss1   = 10000	      # s-squark mass
mstop1 = 10000	      # t-squark mass
msb1   = 10000	      # b-squark mass
msu2   = 10000	      # u-squark mass
msd2   = 10000	      # d-squark mass
msc2   = 10000	      # c-squark mass
mss2   = 10000	      # s-squark mass
mstop2 = 10000	      # t-squark mass
msb2   = 10000	      # b-squark mass
mse1   = 10000	      # selectron1 mass
msne   = 10000	      # electron-sneutrino mass
msmu1  = 10000	      # smuon1 mass
msnmu  = 10000	      # muon-sneutrino mass
mstau1 = 10000	      # stau1 mass    
msntau = 10000	      # tau-sneutrino mass
mse2   = 10000	      # selectron2 mass
msmu2  = 10000	      # smuon2 mass
mstau2 = 10000	      # stau2 mass   
mgg    = 10000	      # gluino mass

wch1  = 0.0000033 eV
wneu1 = 0

# chargino mixing matrix for purely higgsino case!
mu_11  = 0            # chargino mixing matrix
mu_12  = 1            # chargino mixing matrix
mu_21  = -1           # chargino mixing matrix
mu_22  = 0            # chargino mixing matrix
mv_11  = 0            # chargino mixing matrix
mv_12  = 1            # chargino mixing matrix
mv_21  = -1           # chargino mixing matrix
mv_22  = 0            # chargino mixing matrix


# neutralino mixing matrix
 mn_11  = 0
 mn_12  = 0
 mn_13  = 1
 mn_14  = 1
 mn_21  = 0
 mn_22  = -1
 mn_23  = -1
 mn_24  = 0
 mn_31  = 1
 mn_32  = -1
 mn_33  = 0
 mn_34  = 0
 mn_41  = 1
 mn_42  = 0
 mn_43  = 0
 mn_44  = 0

process dec_chargino1m	= "ch1-" => ubar, d, neu1
process dec_chargino1p	= "ch1+" => dbar, u, neu1

process charginopair = e1, E1 => "ch1+", "ch1-"

compile

integrate (dec_chargino1m, dec_chargino1p) { iterations = 1:1000 }

sqrts = 3000 GeV
beams = e1, E1 => circe2 => isr
?keep_beams        = true
!isr_order         = 3
?isr_handler       = true
$isr_handler_mode  = "recoil"
isr_alpha          = 0.0072993
isr_mass           = 0.000511


$circe2_file      = "/cvmfs/clicdp.cern.ch/software/WHIZARD/circe_files/CLIC/3TeVeeMapPB0.67E0.0Mi0.15.circe"
$circe2_design    = "CLIC"
?circe2_polarized = false


! Cuts block
cuts = all M > 4 GeV ["ch1+","ch1-"]


integrate (charginopair) { iterations = 5:10000, 2:10000 }


! Parton shower and hadronization
?ps_fsr_active		= true
?ps_isr_active		= false
?hadronization_active	= true
$shower_method		= "PYTHIA6"
!?ps_PYTHIA_verbose	= true


$ps_PYTHIA_PYGIVE = "MSTJ(28)=0; PMAS(25,1)=120.; PMAS(25,2)=0.3605E-02; MSTJ(41)=2; MSTU(22)=2000; PARJ(21)=0.40000; PARJ(41)=0.11000; PARJ(42)=0.52000; PARJ(81)=0.25000; PARJ(82)=1.90000; MSTJ(11)=3; PARJ(54)=-0.03100; PARJ(55)=-0.00200; PARJ(1)=0.08500; PARJ(3)=0.45000; PARJ(4)=0.02500; PARJ(2)=0.31000; PARJ(11)=0.60000; PARJ(12)=0.40000; PARJ(13)=0.72000; PARJ(14)=0.43000; PARJ(15)=0.08000; PARJ(16)=0.08000; PARJ(17)=0.17000; MSTP(3)=1;MSTP(71)=1;IMSS(1)=11;IMSS(21)=71; IMSS(22)=71"




unstable "ch1-" (dec_chargino1m)
unstable "ch1+" (dec_chargino1p)
