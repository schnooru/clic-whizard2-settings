##################################################
# e+e- -> qqlnu (q=u,d,c,s,b) at 1.4 TeV
# ulrike.schnoor@cern.ch
##################################################

! Model and Process block
model        = SM_ac_CKM

me     = 0
mmu    = 0
mtau   = 0
ms     = 0
mc     = 0
mb     = 0
mtop   = 174 GeV
wtop   = 1.37 GeV
mW     = 80.45 GeV
wW     = 2.071 GeV
mZ     = 91.188 GeV
wZ     = 2.478 GeV
mH     = 125 GeV
wH     = 0.00407 GeV

! anomalous couplings
g1z = 1
ka  = 1
la  = 0

! remove overlap with higher multiplicities
alphas = 0

alias q        = u:d:s:c:b
alias qbar     = U:D:S:C:B
alias lep      = e1:e2:e3:E1:E2:E3
alias neutrino = n1:n2:n3:N1:N2:N3

! Beam block
sqrts = 1400 GeV


process decay_proc = e1, E1 =>  q, qbar, lep, neutrino

! circe2 beam spectrum and ISR
beams              = e1, E1  => circe2 => isr
?keep_beams        = true
!isr_order         = 3
?isr_handler       = true
$isr_handler_mode = "recoil"
isr_alpha          = 0.0072993
isr_mass           = 0.000511
###beams_pol_density  = @(+1), @()
###beams_pol_fraction = 80%, 0%

$circe2_file = "/cvmfs/clicdp.cern.ch/software/WHIZARD/circe_files/CLIC/1.4TeVeeMapPB0.67E0.0Mi0.15.circe"
$circe2_design = "CLIC"
?circe2_polarized = false


! Cuts block
real default_jet_cut = 10 GeV
cuts = all M > default_jet_cut [q,qbar] and all M > 4 GeV [lep, neutrino]  and all M < 100 GeV [q,qbar] and all M < 100 GeV [lep, neutrino] and all Theta > 10 degree [lep] and all Theta < 170 degree [lep] and let subevt @WW = collect[colored:lep:neutrino] in all M > 700 GeV [@WW]
#the m(qq),m(lnu)<100GeV cut removes the single W contribution

! Parton shower and hadronization

?ps_fsr_active		= true
?ps_isr_active		= false
?hadronization_active	= true
$shower_method		= "PYTHIA6"
!?ps_PYTHIA_verbose	= true


$ps_PYTHIA_PYGIVE = "MSTJ(28)=0; PMAS(25,1)=120.; PMAS(25,2)=0.3605E-02; MSTJ(41)=2; MSTU(22)=2000; PARJ(21)=0.40000; PARJ(41)=0.11000; PARJ(42)=0.52000; PARJ(81)=0.25000; PARJ(82)=1.90000; MSTJ(11)=3; PARJ(54)=-0.03100; PARJ(55)=-0.00200; PARJ(1)=0.08500; PARJ(3)=0.45000; PARJ(4)=0.02500; PARJ(2)=0.31000; PARJ(11)=0.60000; PARJ(12)=0.40000; PARJ(13)=0.72000; PARJ(14)=0.43000; PARJ(15)=0.08000; PARJ(16)=0.08000; PARJ(17)=0.17000; MSTP(3)=1;MSTP(71)=1"

integrate (decay_proc)  {iterations = 10:50000:"gw", 5:50000:""}
show(results)

n_events = 100
sample_format = stdhep, ascii
$extension_stdhep = "stdhep"
?unweighted = true

?update_sqme = true
?update_weight = true

simulate (decay_proc) {
checkpoint = 10
alt_setup =
{
g1z = 1.001
ka  = 1
la  = 0
},
{
g1z = 1
ka  = 1.001
la  = 0
},
{
g1z = 1
ka  = 1
la  = 0.001
},
{
g1z = 0.999
ka  = 1
la  = 0
},
{
g1z = 1
ka  = 0.999
la  = 0
},
{
g1z = 1
ka  = 1
la  = -0.001
},
{
g1z = 1.001
ka  = 1.001
la  = 0
},
{
g1z = 1
ka  = 1.001
la  = 0.001
},
{
g1z = 1.001
ka  = 1
la  = 0.001
}
}

