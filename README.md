# CLIC Whizard2 Settings

A collection of relevant Whizard steering files for CLIC provided as examples and with instructions of how to generate CLIC samples.

* Ulrike Schnoor <ulrike.schnoor@cern.ch>
* Philipp Roloff <philipp.roloff@cern.ch>
* Andre Sailer <andre.philippe.sailer@cern.ch>

CLIC is the Compact Linear Collider, a linear e+e- collider with collision energy up to 3 TeV.

Monte Carlo production for CLIC relies on event generators for the e+e-
initial state providing implementations for beam spectra, beam polarisation, 
initial state radiation, and photon-induced processes.

Those requirements are provided by the Whizard generator, interfaced to Pythia 
for the parton shower and hadronization. In this repository we 
share a collection of steering files (Sindarin files) for central processes and
settings used for production at CLIC.


# Description of the selected Sindarin files

We are providing Sindarin scripts for Whizard2.

The file names indicate the process, polarisation, energy stage and model used. 
The collected files represent a selection of settings which we consider to be broadly applicable. 
The user should combine the necessary ingredients from those files.

The names of the files are composed as follows:

`<initial state>2<final state>_<energy stage>_<polarisation mode>_<model>.sin`

*  `<initial state>` can be e+e-, e+gamma, e-gamma, gamma gamma 
*  `<final state>` can be a range of different final states of interest
*  `<energy stage>` can be 380 GeV, 1.5 TeV, or 3 TeV (determines the beamspectra)
*  `<polarisation mode>` can be negPol (P(e-)=80%) or posPol (P(e-)=+80% or noPol (no polarisation mode)
*  `<model>` can be a model from Whizard or an example of how to provide a UFO model


## List of files and the correct cross section

| Process | Filename | cross section | features |
|---------|----------|---------------|----------|
|Inclusive ee->qqqq (incl. WW and ZZ)|`ee2qqqq_3TeV_negPol_SM.sin`| (900 +/-4) fb | neg. pol., 3 TeV beamspectrum |
|ee->ZH; Z->qq| `ee2Hqq_380GeV_posPol_SM.sin`| 67.2 fb | Higgs decay inclusively in Pythia, pos. pol., 380 GeV beamspectrum |
|ee->Hvv| `ee2Hnunu_1.5TeV_negPol_UFO-HC.sin`| (452.5 +/- 0.4) fb | decay chain in Whizard, load a UFO model, 1.5 TeV beamspectrum |
|ee->tt| `ee2ttbar_380GeV_noPol_SM-top-anom.sin`|372.3 fb| loading a non-SM Whizard model, no polarisation, top decay in Pythia |
|Inclusive egamma->qqgamma (incl. single-Z)| `egamEPA2eqq_1.5TeV_noPol_SM.sin`|(12420 +/- 200) fb| EPA photon, m(qq) cut for more efficiency |
|Inclusive gammagamma->qqqq (incl. WW and ZZ)| `gamEPAgamBS2qqqq_3TeV_noPol_SM.sin`| (5356 +/- 50) fb  | EPA and Beamstrahlung photon, no ISR, m(qqqq) cut|

The files demonstrate the hadronization parameters used in Pythia6 (OPAL tune).

In each file with ISR, we set Whizard's ISR handler to use the "recoil" mode which provides a transverse momentum to the ISR photon.

Where relevant, the resonance history is set up with the settings found to be applicable for CLIC. The resonance history passes to the parton shower the information about resonances (in particular W, Z) in the matrix element.

Top and Higgs bosons can decay in Pythia (narrow width approximation) or within Whizard in decay chains. For other resonances (W,Z) it is more accurate to generate the final state (e.g. qq) directly and to use the resonance history. Another option would be to use a `$restrictions` but this is not gauge invariant, so has to be handled with care. See the Whizard manual (https://whizard.hepforge.org/manual/manual006.html#sec134) for more information.

Default CLIC parameter settings are used. Cut settings ensure orthogonality with photon-induced processes. In some cases, to increase the efficiency of the MC generation, m(qq) or m(qqqq) cuts are used. It depends on the process if these cuts are acceptable.



## Cut settings
In general, default cuts are used which ensure the orthogonality between e+e- and photon-induced processes and remove singularities. In addition, in some files we have added cuts on the outgoing qq or qqqq invariant masses which is meant as an example for increasing the efficiency of sample production but would not be applicable for all inclusive cases. This is done in the files: `ee2qqqq_3TeV_negPol_SM.sin`, `gamEPAgamBS2qqqq_3TeV_noPol_SM.singamEPAgamBS2qqqq_3TeV_noPol_SM.sin`, and `egamEPA2eqq_1.5TeV_noPol_SM.sin`.

It goes without saying that for specific analyses, the cut settings might have to be reconsidered.

## Beamspectra for CLIC: 
The files for the beam spectra can be found here:
https://whizard.hepforge.org/circe_files/CLIC/
or on cvmfs in `/cvmfs/clicdp.cern.ch/software/WHIZARD/circe_files/CLIC/`.

The order of the beam particles in the Sindarin `beams` statement and in the initial state of the `process` definition must always be identical to the order used in these beam spectrum files (line 8).

For the energy stage at 1.5 TeV, the beamspectrum from 1.4 TeV can be used. For this purpose, line 4 of the file for 1.4 TeV needs to be adjusted to contain 1.5 TeV:

 `'CLIC' 1400` ->  `'CLIC' 1500`



## Photon-induced processes

There are 2 types of photon induced initial states:
+ Equivalent Photon Approximation (EPA): modeled in Whizard analytically
+ Beamstrahlung photons: obtained from Beam Spectrum event files

For this reason, there are 4 different combinations of e+gamma/e-gamma and gammagamma initial states each:
+ e- gamma(BS)
+ e- gamma(EPA)
+ e+ gamma(BS)
+ e+ gamma(EPA)

and

+ gamma(EPA) gamma(EPA)
+ gamma(BS) gamma(BS)
+ gamma(EPA) gamma(BS) 
+ gamma(BS) gamma(EPA)


For the latter two cases the difference is whether the photon originates from the electron or positron beam.

Note that EPA and ISR are not compatible at this point, so if EPA is applied, do not use ISR.

### Normalization of beamstrahlung-induced processes
For processes with beamstrahlung photons in the initial state, normalisation factors are listed here (from https://twiki.cern.ch/twiki/bin/view/CLIC/MonteCarloSamplesForTheHiggsPaper):


| ratio | 380 GeV | 1.5 TeV| 	3 TeV|
| ------ | ------ |------ | ------ |
|L(ee) / L(ee) |	1.0  |	1.0   |	1.0|
|L(γe) / L(ee) |	0.55 |	0.75  |	0.79|
|L(γγ) / L(ee) |	0.36 |	0.64  |	0.69|

! This is not needed for EPA photons !

Numbers can be found here: http://clic-beam-beam.web.cern.ch/clic-beam-beam/380gev_l6_bx8mm_beam6.html (for 380GeV default - other options in the corresponding pages)

## Event generation
In order to generate events in the stdhep format, add to the end of the file:
```
n_events = 10000
sample_format = stdhep 
$extension_stdhep = "stdhep"
simulate (decay_proc) {checkpoint = 100}
```
where `decay_proc` should be the name of the process as is the case in most of the provided files.
More documentation in the Whizard manual.

## Processes with hard photons in the final state
For processes with hard photons in the final state, a presciption is described 
in this publication:
https://cds.cern.ch/record/2716332

# Resources
CLIC software twiki: https://twiki.cern.ch/twiki/bin/view/CLIC/CLICSoftwareComputing

MC production for CLICdet: https://twiki.cern.ch/twiki/bin/view/CLIC/MonteCarloSamplesForCLICdet

Whizard: https://whizard.hepforge.org/

CLICdet Delphes card:
+ Available in the DELPHES repository: https://github.com/delphes
+ Short instructions twiki: https://twiki.cern.ch/twiki/bin/view/CLIC/DelphesMadgraphForBSMReport
+ Validation and manual: https://arxiv.org/abs/1909.12728